package com.training.spring.tradeapi.controller;

import java.util.Collection;

import com.training.spring.tradeapi.entities.Stock;
import com.training.spring.tradeapi.service.StockService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StockController {
    @Autowired
    private StockService stockService;

    @PostMapping("/stock")
    public void addStock(@RequestBody Stock stock){
        stockService.addStock(stock);
    }

    // @RequestMapping(path="/stock/{id}", method = RequestMethod.DELETE)
    // public void removeCar(@PathVariable("id") String id) {
    //     stockService.deleteStock(id);
    // }

    @GetMapping("/stock")
    public Collection<Stock> queryStock(
        @RequestParam(value = "id", required = false) String id,
        @RequestParam(value = "ticker", required = false) String ticker,
        @RequestParam(value = "price", required = false) Double price,
        @RequestParam(value = "quant", required = false) Double quant,
        @RequestParam(value = "date", required = false) String date,
        @RequestParam(value = "status", required = false) String status
    ){
        System.out.println("" + id + " | " + ticker + " | " + " | " + price + " | " + quant + " | " + date + " | " + status);
        return stockService.queryStock(id,quant,price,ticker,status);
    }
}