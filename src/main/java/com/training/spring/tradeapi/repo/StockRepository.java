package com.training.spring.tradeapi.repo;


import com.training.spring.tradeapi.entities.Stock;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface StockRepository extends MongoRepository<Stock, ObjectId>{
    
}