package com.training.spring.tradeapi.service;

import java.util.Collection;

import com.training.spring.tradeapi.entities.Stock;
public interface StockService {
    Collection<Stock> getAllStocks();
    void addStock(Stock stock);
    Stock getStock(String id);
    void deleteStock(String id);
    //Collection<Stock> queryStock();
    Collection<Stock> queryStock(String id, Double stockQuanity, Double price, String stockTicker, String status);
}