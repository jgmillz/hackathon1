package com.training.spring.tradeapi.service;

import java.util.Collection;
import java.util.Optional;

import com.training.spring.tradeapi.entities.Stock;
import com.training.spring.tradeapi.repo.StockRepository;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseStatus;

@Service
public class StockServiceImpl implements StockService{

    @Autowired
    private MongoTemplate mongoTemplate;
    @Autowired
    private StockRepository repo;

	@Override
	public void addStock(Stock stock) {
        repo.insert(stock);
	}

    @Override
    public Collection<Stock> getAllStocks() {
        return repo.findAll();
    }

    @Override
    public Stock getStock(String id){
        System.out.println(id);
        Optional<Stock> currentStock = repo.findById(new ObjectId(id));
        if(currentStock.isPresent()){
            return currentStock.get();
        }
        System.out.println("Not In Database");
        return null;
    }

    @Override
    @ResponseStatus(HttpStatus.OK)
    public void deleteStock(String id) {
       repo.deleteById(new ObjectId(id));
    }

    @Override
    public Collection<Stock> queryStock(String id, Double stockQuanity, Double price, String stockTicker, String status) {
        Query dynamicQuery = new Query();
        if(id != null){
            System.out.println(id);
            Criteria nameCriteria = Criteria.where("id").is(id);
            dynamicQuery.addCriteria(nameCriteria);
        }
        // Change this later
        if(stockQuanity != null){
            System.out.println(stockQuanity);
            Criteria qCriteria = Criteria.where("stockQuantity").is(stockQuanity);
            dynamicQuery.addCriteria(qCriteria);
        }
        // Change this later
        if(price != null){
            System.out.println(price);
            Criteria pCriteria = Criteria.where("price").is(price);
            dynamicQuery.addCriteria(pCriteria);
        }
        if(stockTicker != null){
            System.out.println(stockTicker);
            Criteria tCriteria = Criteria.where("stockTicker").is(stockTicker);
            dynamicQuery.addCriteria(tCriteria);
        }
        if(status != null){
            System.out.println(status);
            Criteria sCriteria = Criteria.where("status").is(status);
            dynamicQuery.addCriteria(sCriteria);
        }
        Collection<Stock> result = mongoTemplate.find(dynamicQuery, Stock.class);
        System.out.println(result);
        return result;
    }
    
    
}