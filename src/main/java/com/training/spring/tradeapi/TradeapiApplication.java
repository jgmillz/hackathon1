package com.training.spring.tradeapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TradeapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TradeapiApplication.class, args);
	}

}
